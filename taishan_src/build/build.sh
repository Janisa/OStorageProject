#! /bin/bash 

OUTDIR=../output 

if [ -z $1 ]; then 
    
	echo "\n Usage: build.sh agrs"
	echo "\t agrs list:"
	echo "\t all: compiler all mode."
	echo "\t clean: clear all temp files."
	echo "\t cleanall: clear project temp files."
	echo "\t perf: eg.compiler perf mode.\n"
	exit
fi
cp CMakeLists.txt ../
mkdir -p  $OUTDIR && ld --verbose >$OUTDIR/ldscript.lds
sed -i '246d;1,13d' $OUTDIR/ldscript.lds && sed -i '156 r ldcfg' $OUTDIR/ldscript.lds
case $1 in

	all)
		cmake ..&&make
		;;
	clean)
		make clean
		;;
	cleanall)
		rm -rf $OUTDIR/
		find . |grep -v build.sh | grep -v CMakeLists.txt | grep -v ldcfg | xargs rm -rf 2>/dev/null &1>/dev/null
		;;
	*)
		cmake ..&& make $1
		;;
esac 

rm -rf ../CMakeLists.txt