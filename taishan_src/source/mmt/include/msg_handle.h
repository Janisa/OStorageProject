#ifndef __MSG_HANDLE_H__
#define __MSG_HANDLE_H__
#include "os_defines.h"

int tcp_connect_server(const char* server_ip, int port);
s32 Msg_Initial_log();
void Msg_socket_read_cb(int fd, short events, void*arg);
void Msg_accept_cb(int fd, short events, void*arg);
void Msg_socket_read_cb(int fd, short events, void*arg);
void * msg_handler(void *pMsgInfo);
int Msg_tcp_server_init(int port, int listen_num);


#endif

