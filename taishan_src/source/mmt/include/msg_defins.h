/***********************************************************************************
 * 文 件 名   : msg_defins.h
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年5月8日
 * 文件描述   : 消息分发模块公共头文件
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   : 
 * 修改日志   : 
***********************************************************************************/

#ifndef __MSG_DEFINES_H__
#define __MSG_DEFINES_H__

#define MSG_MODE_NAME  "mmt"
#define MSG_MODE_PID   3  

#define msg_log(loglevel,fmt, args...)\
            Debug_Log(MSG_MODE_PID,loglevel,__func__,__LINE__,__FILE__,fmt,##args)

#endif
