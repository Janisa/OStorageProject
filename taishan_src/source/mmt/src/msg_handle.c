/***********************************************************************************
 * 文 件 名   : msg_handle.c
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年5月8日
 * 文件描述   : 消息处理实现
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   :
 * 修改日志   :
***********************************************************************************/

#include "os_log.h"
#include"msg_other.h"
#include"msg_defins.h"
#include<event.h>
#include"msg_handle.h"
#include"os_msghead.h"
#include"os_socket.h"
#include <arpa/inet.h>

int g_Server_Client_Info[MSG_SERCVER_ACCESS_NUM][2] = {0};
int g_Node_Pid = 1;
/*****************************************************************************
 * 函 数 名  : Mmt_Initial_log
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年5月8日
 * 函数功能  : 初始化日志配置
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
s32 Msg_Initial_log()
{
    u32 iRet = RET_ERR;
    Debug_InitLogFromMode(MSG_MODE_PID, MSG_MODE_NAME, LOG_INFO);
    OS_ASSERT_PTR(iRet, RET_ERR);
    return RET_OK;
}

/*****************************************************************************
 * 函 数 名  : Msg_socket_read_cb
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年5月8日
 * 函数功能  : 事件触发后的回调函数
 * 输入参数  : int fd        触发事件的socket
               short events  事件
               void*arg      响应参数
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
void Msg_socket_read_cb(int fd, short events, void*arg)
{
    char pMsgInfo[MSG_DATA_LEN];
    MSGHEAD msg;
    struct event*ev = (struct event*) arg;
    memset(pMsgInfo, 0, MSG_DATA_LEN);
    int len = socket_recv_exact_nbytes(fd, pMsgInfo, MSG_DATA_LEN);

    if (len <= 0)
    {
        msg_log(LOG_INFO, "some error happen when read\n");
        event_free(ev);
        close(fd);
        return;
    }
    msg_log(LOG_INFO, "data:%s,len=%d",pMsgInfo, len);
    Os_DecodeMsg(&msg, pMsgInfo);
    msg_log(LOG_INFO, "SendPid=%d,RecvPid=%d,SendNode=%d,RecvNode=%d,OperCode=%lu,RquesFun=%lu,MsgBuffLen=%llu,RequesData=%s",
            msg.uiSendPid,
            msg.uiRecvPid,
            msg.uiSendNode,
            msg.uiRecvNode,
            msg.uiOperCode,
            msg.uiRquesFun,
            msg.uiMsgBuffLen,
            msg.RequesData);
    return;
}


/*****************************************************************************
 * 函 数 名  : Msg_tcp_server_init
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年5月8日
 * 函数功能  : 初始化socket
 * 输入参数  : int port        输入socket
               int listen_num  最大连接数
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
int Msg_tcp_server_init(int port, int listen_num)
{
    int errno_save;
    evutil_socket_t listener;

    listener = socket(AF_INET, SOCK_STREAM, 0);
    if (RET_ERR == listener)
    {
        msg_log(LOG_ERROR, "initial socket fail.");
        return RET_ERR;
    }

    //允许多次绑定同一个地址，要用在socket和bind之间
    evutil_make_listen_socket_reuseable(listener);

    struct sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = 0;
    sin.sin_port = htons(port);
    if (bind(listener, (struct sockaddr*) &sin, sizeof(sin)) < 0)
    {
        errno_save = errno;
        evutil_closesocket(listener);
        errno = errno_save;
        msg_log(LOG_ERROR, "bind initial fail,err(%d).", errno_save);
        return RET_ERR;
    }
    if (listen(listener, listen_num) < 0)
    {
        errno_save = errno;
        evutil_closesocket(listener);
        errno = errno_save;
        msg_log(LOG_ERROR, "listen initial fail,err(%d).", errno_save);
        return RET_ERR;
    }
    evutil_make_socket_nonblocking(listener);
    return listener;
}

/*****************************************************************************
 * 函 数 名  : Msg_accept_cb
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年5月8日
 * 函数功能  : 监听新客户端连接事件
 * 输入参数  : int fd        初始化socket
               short events  事件id
               void*arg      响应参数
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
void Msg_accept_cb(int fd, short events, void*arg)
{
    evutil_socket_t sockfd;

    struct sockaddr_in client;
    socklen_t len = sizeof(client);

    sockfd = accept(fd, (struct sockaddr*) &client, &len);
    evutil_make_socket_nonblocking(sockfd);

    msg_log(LOG_DEBUG, "accept a client %d.", sockfd);
    struct event_base* base = (struct event_base*) arg;

    //动态创建一个event结构体
    struct event* ev = event_new(NULL, -1, 0, NULL, NULL);
    //将动态创建的结构体作为event的回调参数
    event_assign(ev, base, sockfd, EV_READ | EV_PERSIST, Msg_socket_read_cb, (void*) ev);
    event_add(ev, NULL);
}
/*****************************************************************************
 * 函 数 名  : tcp_connect_server
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年5月12日
 * 函数功能  : 连接到服务器
 * 输入参数  : const char* server_ip  服务器IP
               int port               端口
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int tcp_connect_server(const char* server_ip, int port)
{
    int sockfd, status;
    struct sockaddr_in server_addr;

    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);

    status = inet_aton(server_ip, &(server_addr.sin_addr));
    if(status == 0)
    {
        errno = EINVAL;
        return -1;
    }

    sockfd = socket(PF_INET, SOCK_STREAM, 0);
    if(sockfd == -1)
    {
        return sockfd;
    }

    status = connect(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr));
    if(status == -1)
    {
        int save_errno = errno;
        close(sockfd);
        errno = save_errno;
        return -1;
    }

    evutil_make_socket_nonblocking(sockfd);
    return sockfd;

}

