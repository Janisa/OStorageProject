#include<stdio.h>
#include<string.h>
#include<errno.h>
#include<stdlib.h>
#include<unistd.h>
#include<event.h>

#include"msg_handle.h"
#include"msg_pool.h"
#include "os_defines.h"
#include "msg_other.h"



int main(int argc, char**argv)
{
    s32 iRet = RET_ERR;

    iRet = Msg_Initial_log();
    if(RET_ERR == iRet)
    {
        exit(RET_ERR);
    }
    iRet = msg_Init_Send_to_mode();
    if(RET_ERR == iRet)
    {
        exit(RET_ERR);
    }

    int listener = Msg_tcp_server_init(MSG_SERCVER_PORT, MSG_SERCVER_ACCESS_NUM);

    OS_ASSERT_PTR_RET_NO_LOG(listener, RET_ERR);

    struct event_base* base = event_base_new();

    //添加监听客户端连接请求事件
    struct event* ev_listen = event_new(base, listener, EV_READ | EV_PERSIST, Msg_accept_cb, base);
    event_add(ev_listen, NULL);

    //开始循环工作
    event_base_dispatch(base);
    return RET_OK;
}

