/***********************************************************************************
 * 文 件 名   : msg_poll.c
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年5月8日
 * 文件描述   : 消息发送处理
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   : 
 * 修改日志   : 
***********************************************************************************/
#include "os_threadpool.h"
#include<unistd.h>
#include<msg_pool.h>
#include "msg_other.h"
static threadpool_t* g_Send_to_mode_pool = NULL;

/*****************************************************************************
 * 函 数 名  : msg_Init_Send_to_mode
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年5月8日
 * 函数功能  : 初始化发送数据到其他模块线程池
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
s32 msg_Init_Send_to_mode()
{
	g_Send_to_mode_pool = OS_Threadpool_create(1, MSG_SERCVER_ACCESS_NUM,MSG_SERCVER_ACCESS_NUM * 2);
	OS_ASSERT_PTR(g_Send_to_mode_pool,RET_ERR);
    return RET_OK;
	
}