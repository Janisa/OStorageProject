/***********************************************************************************
 * 文 件 名   : os_time.h
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年7月8日
 * 文件描述   : 时间处理工具
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   : 
 * 修改日志   : 
***********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/timeb.h>

#include "os_log.h"
#include "os_time.h"

#define TIME_FORMAT "%Y%m%d%H%M%S"

uint32_t OS_time_get_sec()
{
    time_t t;
    t = time(NULL);
    if (t == -1) {
        OS_LOG(LOG_ERROR,"time failed %d:%s", errno, strerror(errno));
    }
    return t;
}

char *OS_time_get_sec_str()
{
    time_t t;
    struct tm *tm;
    t = time(NULL);
    if (t == -1) {
        OS_LOG(LOG_ERROR,"time failed %d:%s", errno, strerror(errno));
    }
    tm = localtime(&t);
    if (!tm) {
        OS_LOG(LOG_ERROR,"localtime failed %d:%s", errno, strerror(errno));
    }
    return asctime(tm);
}

char *OS_time_get_str_human(char *str, int len)
{
    struct time_info ti;
    if (-1 == OS_time_get_info(&ti)) {
        return NULL;
    }
    snprintf(str, len, "%04d%02d%02d%02d%02d%02d",
             ti.year, ti.mon, ti.day, ti.hour, ti.min, ti.sec);
    return str;
}

char *OS_time_get_str_human_by_utc(uint32_t utc, char *str, int len)
{
    struct time_info ti;
    if (-1 == OS_time_get_info_by_utc(utc, &ti)) {
        return NULL;
    }
    snprintf(str, len, "%04d%02d%02d%02d%02d%02d",
             ti.year, ti.mon, ti.day, ti.hour, ti.min, ti.sec);
    return str;
}

uint64_t OS_time_get_usec()
{
    struct timeval tv;
    if (-1 == gettimeofday(&tv, NULL)) {
        OS_LOG(LOG_ERROR,"gettimeofday failed %d:%s", errno, strerror(errno));
        return -1;
    }
    return (uint64_t)(((uint64_t)tv.tv_sec)*1000*1000 + (uint64_t)tv.tv_usec);
}

uint64_t _time_clock_gettime(clockid_t clk_id)
{
    struct timespec ts;
    if (-1 == clock_gettime(clk_id, &ts)) {
        OS_LOG(LOG_ERROR,"clock_gettime failed %d:%s", errno, strerror(errno));
        return -1;
    }
    return (uint64_t)(((uint64_t)ts.tv_sec*1000*1000*1000) + (uint64_t)ts.tv_nsec);
}

uint64_t OS_time_get_msec()
{
    struct timeb tb;
    ftime(&tb);
    return (uint64_t)(((uint64_t)tb.time) * 1000 + (uint64_t)tb.millitm);
}

uint64_t OS_time_get_nsec()
{
    return _time_clock_gettime(CLOCK_REALTIME);
}

uint64_t OS_time_get_nsec_bootup()
{
    return _time_clock_gettime(CLOCK_MONOTONIC);
}

char *OS_time_get_msec_str(char *str, int len)
{
    char date_fmt[20];
    char date_ms[4];
    struct timeval tv;
    struct tm now_tm;
    int now_ms;
    time_t now_sec;
    if (len < 24) {
        OS_LOG(LOG_ERROR,"time string len must bigger than 24");
        return NULL;
    }
    if (-1 == gettimeofday(&tv, NULL)) {
        OS_LOG(LOG_ERROR,"gettimeofday failed %d:%s", errno, strerror(errno));
        return NULL;
    }
    now_sec = tv.tv_sec;
    now_ms = tv.tv_usec/1000;
    if (NULL == localtime_r(&now_sec, &now_tm)) {
        OS_LOG(LOG_ERROR,"localtime_r failed %d:%s", errno, strerror(errno));
        return NULL;
    }

    strftime(date_fmt, 20, "%Y-%m-%d %H:%M:%S", &now_tm);
    snprintf(date_ms, sizeof(date_ms), "%03d", now_ms);
    snprintf(str, len, "%s.%s", date_fmt, date_ms);
    return str;
}

int OS_time_sleep_ms(uint64_t ms)
{
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = ms*1000;
    return select(0, NULL, NULL, NULL, &tv);
}

int OS_time_get_info_by_utc(uint32_t utc, struct time_info *ti)
{
    struct timeval tv;
    struct timezone tz;
    struct tm *now;
    char date_fmt[20] = {0};
    char date_ms[4] = {0};

    now = localtime((time_t *)&utc);
    if (!now) {
        return -1;
    }
    if (-1 == gettimeofday(&tv, &tz)) {
        return -1;
    }

    ti->utc = (uint32_t)utc;
    ti->year = now->tm_year + 1900;
    ti->mon = now->tm_mon + 1;
    ti->day = now->tm_mday;
    ti->hour = now->tm_hour;
    ti->min = now->tm_min;
    ti->sec = now->tm_sec;
    ti->timezone = (-tz.tz_minuteswest) / 60;

    strftime(date_fmt, sizeof(ti->str), TIME_FORMAT, now);
    snprintf(date_ms, sizeof(date_ms), "%03d", ti->msec);
    snprintf(ti->str, sizeof(ti->str), "%s%s", date_fmt, date_ms);

    return 0;
}

int OS_time_get_info(struct time_info *ti)
{
    time_t utc;
    struct timeval tv;
    struct timezone tz;
    struct tm *now;
    char date_fmt[20] = {0};
    char date_ms[4] = {0};

    if (-1 == time(&utc)) {
        return -1;
    }
    if (-1 == gettimeofday(&tv, &tz)) {
        return -1;
    }
    now = localtime(&utc);
    if (!now) {
        return -1;
    }

    ti->utc = (uint32_t)utc;
    ti->utc_msec = ((uint64_t)utc)*1000 + ti->msec;
    ti->year = now->tm_year + 1900;
    ti->mon = now->tm_mon + 1;
    ti->day = now->tm_mday;
    ti->hour = now->tm_hour;
    ti->min = now->tm_min;
    ti->sec = now->tm_sec;
    ti->msec = tv.tv_usec/1000;
    ti->timezone = (-tz.tz_minuteswest) / 60;

    strftime(date_fmt, sizeof(ti->str), TIME_FORMAT, now);
    snprintf(date_ms, sizeof(date_ms), "%03d", ti->msec);
    snprintf(ti->str, sizeof(ti->str), "%s%s", date_fmt, date_ms);

    return 0;
}

int time_set_info(struct time_info *ti)
{
    time_t timep;
    struct tm tm;
    struct timeval tv, tv1;
    struct timezone tz;
    tm.tm_sec = ti->sec;
    tm.tm_min = ti->min;
    tm.tm_hour = ti->hour;
    tm.tm_mday = ti->day;
    tm.tm_mon = ti->mon - 1;
    tm.tm_year = ti->year - 1900;

    timep = mktime(&tm);
    tv.tv_sec = timep;
    tv.tv_usec = 0;

    gettimeofday(&tv1, &tz);
    tz.tz_minuteswest = -(ti->timezone) * 60;
    if (-1 == settimeofday(&tv, &tz)) {
        return -1;
    }
    return 0;
}

bool OS_time_passed_sec(int sec)
{
    bool ret = false;
    static uint32_t last_sec = 0;
    static uint32_t now_sec = 0;
    now_sec = OS_time_get_sec();
    if (last_sec == 0) {
        last_sec = now_sec;
    }
    if (now_sec - last_sec >= (uint32_t)sec) {
        ret = true;
        last_sec = now_sec;
    } else {
        ret = false;
    }
    return ret;
}
