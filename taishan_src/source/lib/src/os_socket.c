/***********************************************************************************
 * 文 件 名   : os_socket.c
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年4月1日
 * 文件描述   : 套接字常用操作封装
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   :
 * 修改日志   :
***********************************************************************************/

#include <sys/socket.h>
#include <arpa/inet.h>
#include "os_socket.h"
#include "os_defines.h"


/*****************************************************************************
 * 函 数 名  : socket_init
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 服务端活客户端socket初始化
 * 输入参数  : int domain                    IP域
               int type                      套接字类型
               int protocol                  端口
               struct sockaddr_in *pAddress  ip地址信息指针
               char *pstrIp                  addrip
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
int socket_init(int domain, int type, int protocol, struct sockaddr_in *pAddress , char *pstrIp)
{
    OS_ASSERT_PTR_RET_NO_LOG(pAddress, RET_ERR);
    OS_ASSERT_PTR_RET_NO_LOG(pstrIp, RET_ERR);
    memset(pAddress, 0, sizeof(struct sockaddr_in));
    (*pAddress).sin_family = domain;
    //IP地址设置成INADDR_ANY,让系统自动获取本机的IP地址。
    (*pAddress).sin_addr.s_addr = (strncmp(pstrIp, "INADDR_ANY", strlen("INADDR_ANY")) == 0) ? htonl(INADDR_ANY) : inet_addr(pstrIp);
    (*pAddress).sin_port = htons(protocol);//设置的端口为DEFAULT_PORT
    return socket(domain, type, 0);
}
/*****************************************************************************
 * 函 数 名  : socket_bind
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 服务端或客户端绑定套接字
 * 输入参数  : int socket_fd                socket套接字
               struct sockaddr_in pAddress  address信息
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
int socket_bind(int socket_fd, struct sockaddr_in pAddress)
{
    //将本地地址绑定到所创建的套接字上
    return bind(socket_fd, (struct sockaddr*)&pAddress, sizeof(struct sockaddr_in));
}
/*****************************************************************************
 * 函 数 名  : socket_listen
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 服务端绑定监听套接字
 * 输入参数  : int socket_fd     套接字
               ssize_t maxcount  最大监听数目
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
int socket_listen(int socket_fd, ssize_t maxcount)
{
    //开始监听套接字
    return listen(socket_fd, maxcount);
}
/*****************************************************************************
 * 函 数 名  : socket_connect
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 客户端连接到服务端
 * 输入参数  : int socket_fd                套接字
               struct sockaddr_in pAddress  服务端地址
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
int socket_connect(int socket_fd, struct sockaddr_in pAddress)
{
    //客户端连接套接字
    return connect(socket_fd, (struct sockaddr*)&pAddress, sizeof(struct sockaddr_in));
}
/*****************************************************************************
 * 函 数 名  : socket_accept
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 接受客户端的连接
 * 输入参数  : int socket_fd                套接字
               struct sockaddr_in *cliaddr  存放客户端信息
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
int socket_accept(int socket_fd, struct sockaddr_in *cliaddr)
{
    socklen_t addrlen;
    OS_ASSERT_PTR_RET_NO_LOG(cliaddr, RET_ERR);
    addrlen = sizeof(struct sockaddr_in);
    memset(cliaddr, 0, addrlen);
    //开始接受套接字连接
    return accept(socket_fd, (struct sockaddr *)cliaddr, &addrlen);
}

/*****************************************************************************
 * 函 数 名  : socket_recv
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 在指定描述符中读取指定大小数据到缓存中
 * 输入参数  : int fd        套接字句柄
               void *buf     输出缓存地址
               size_t count  缓存最大限制
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
ssize_t socket_recv(int fd, void *buf, size_t count)
{
    OS_ASSERT_PTR_RET_NO_LOG(buf, RET_ERR);
    return read(fd, buf, count);
}
/*****************************************************************************
 * 函 数 名  : socket_send
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 想指定描述符写入缓存中的数据
 * 输入参数  : int fd           描述符
               const void *buf  输入缓存
               size_t count     输入缓存最大限制
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
ssize_t socket_send(int fd, const void *buf, size_t count)
{
    OS_ASSERT_PTR_RET_NO_LOG(buf, RET_ERR);
    return write(fd, buf, count);
}
/*****************************************************************************
 * 函 数 名  : socket_recv_exact_nbytes
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 在指定描述符中一直读取指定大小的缓存为止
 * 输入参数  : int fd        句柄描述符
               void *buf     输出缓存
               size_t count  缓存大小限制
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
ssize_t socket_recv_exact_nbytes(int fd, void *buf, size_t count)
{
    ssize_t ret;
    ssize_t total = 0;

    OS_ASSERT_PTR_RET_NO_LOG(buf, RET_ERR);

    while (total != count)
    {
        ret = read(fd, buf + total, count - total);
        if (ret <= 0)
        {
            break;
        }
        else
        {
            total += ret;
        }
    }

    return total;
}
/*****************************************************************************
 * 函 数 名  : socket_send_exact_nbytes
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 向指定描述符中输入指定字符为止
 * 输入参数  : int fd           描述符
               const void *buf  输入缓存
               size_t count     缓存大小、
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
ssize_t socket_send_exact_nbytes(int fd, const void *buf, size_t count)
{
    ssize_t ret;
    ssize_t total = 0;

    OS_ASSERT_PTR_RET_NO_LOG(buf, RET_ERR);

    while (total != count)
    {
        ret = write(fd, buf + total, count - total);
        if (ret <= 0)
        {
            break;
        }
        else
        {
            total += ret;
        }
    }

    return total;
}
/*****************************************************************************
 * 函 数 名  : socket_exit
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 关闭指定的套接字
 * 输入参数  : int fd  套接字
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
int socket_exit(int fd)
{
    return close(fd);
}


