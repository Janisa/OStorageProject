/***********************************************************************************
 * 文 件 名   : os_lock.c
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年7月8日
 * 文件描述   : 资源并发管理
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   : 
 * 修改日志   : 
***********************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sched.h>
#include <pthread.h>
#include <semaphore.h>

#include "os_log.h"
#include "os_lock.h"


/******************************************************************************
 * spin lock APIs
 *****************************************************************************/
#if ( __i386__ || __i386 || __amd64__ || __amd64 )
#define cpu_pause() __asm__ ("pause")
#else
#define cpu_pause()
#endif

#define atomic_cmp_set(lock, old, set) \
    __sync_bool_compare_and_swap(lock, old, set)

static long g_ncpu = 1;
/*****************************************************************************
 * 函 数 名  : OS_spin_lock_init
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : spin_lock锁初始化
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : spin_lock_t
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
spin_lock_t *OS_spin_lock_init()
{
    spin_lock_t *lock = (spin_lock_t *)calloc(1, sizeof(spin_lock_t));
    if (!lock) {
        OS_LOG(LOG_ERROR,"malloc spin_lock_t failed:%d", errno);
        return NULL;
    }
    g_ncpu = sysconf(_SC_NPROCESSORS_ONLN);
    return lock;
}

/*****************************************************************************
 * 函 数 名  : OS_spin_lock
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 加锁
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : spin_lock_t
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_spin_lock(spin_lock_t *lock)
{
    int spin = 2048;
    int value = 1;
    int i, n;
    for ( ;; ) {
        if (*lock == 0 && atomic_cmp_set(lock, 0, value)) {
            return 0;
        }
        if (g_ncpu > 1) {
            for (n = 1; n < spin; n <<= 1) {
                for (i = 0; i < n; i++) {
                    cpu_pause();
                }
                if (*lock == 0 && atomic_cmp_set(lock, 0, value)) {
                    return 0;
                }
            }
        }
        sched_yield();
    }
    return 0;
}
/*****************************************************************************
 * 函 数 名  : OS_spin_unlock
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 解锁
 * 输入参数  : spin_lock_t *lock  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_spin_unlock(spin_lock_t *lock)
{
    *(lock) = 0;
    return 0;
}
/*****************************************************************************
 * 函 数 名  : OS_spin_trylock
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 尝试加锁
 * 输入参数  : spin_lock_t *lock  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_spin_trylock(spin_lock_t *lock)
{
    return (*(lock) == 0 && atomic_cmp_set(lock, 0, 1));
}
/*****************************************************************************
 * 函 数 名  : OS_spin_lock_deinit
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 销毁锁柜子
 * 输入参数  : spin_lock_t *lock  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void OS_spin_lock_deinit(spin_lock_t *lock)
{
    if (!lock) {
        return;
    }
    free(lock);
}

/******************************************************************************
 * mutex lock APIs
 *****************************************************************************/
/*****************************************************************************
 * 函 数 名  : OS_mutex_lock_init
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : mutex锁初始化
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : mutex_lock_t
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
mutex_lock_t *OS_mutex_lock_init()
{

    pthread_mutex_t *lock = (pthread_mutex_t *)calloc(1,
                                               sizeof(pthread_mutex_t));
    if (!lock) {
        OS_LOG(LOG_ERROR,"malloc pthread_mutex_t failed:%d", errno);
        return NULL;
    }
    pthread_mutex_init(lock, NULL);
    return lock;
}
/*****************************************************************************
 * 函 数 名  : OS_mutex_lock_deinit
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 销毁说柜子
 * 输入参数  : mutex_lock_t *ptr  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void OS_mutex_lock_deinit(mutex_lock_t *ptr)
{
    if (!ptr) {
        return;
    }
    pthread_mutex_t *lock = (pthread_mutex_t *)ptr;
    int ret = pthread_mutex_destroy(lock);
    if (ret != 0) {
        switch (ret) {
        case EBUSY:
            OS_LOG(LOG_ERROR,"the mutex is currently locked.");
            break;
        default:
            OS_LOG(LOG_ERROR,"pthread_mutex_trylock error:%s.", strerror(ret));
            break;
        }
    }
    free(lock);
}
/*****************************************************************************
 * 函 数 名  : OS_mutex_trylock
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 尝试加锁
 * 输入参数  : mutex_lock_t *ptr 锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_mutex_trylock(mutex_lock_t *ptr)
{
    if (!ptr) {
        return -1;
    }
    pthread_mutex_t *lock = (pthread_mutex_t *)ptr;
    int ret = pthread_mutex_trylock(lock);
    if (ret != 0) {
        switch (ret) {
        case EBUSY:
            OS_LOG(LOG_ERROR,"the mutex could not be acquired"
                   " because it was currently locked.");
            break;
        case EINVAL:
            OS_LOG(LOG_ERROR,"the mutex has not been properly initialized.");
            break;
        default:
            OS_LOG(LOG_ERROR,"pthread_mutex_trylock error:%s.", strerror(ret));
            break;
        }
    }
    return ret;
}
/*****************************************************************************
 * 函 数 名  : OS_mutex_lock
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 加锁
 * 输入参数  : mutex_lock_t *ptr  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_mutex_lock(mutex_lock_t *ptr)
{
    if (!ptr) {
        return -1;
    }
    pthread_mutex_t *lock = (pthread_mutex_t *)ptr;
    int ret = pthread_mutex_lock(lock);
    if (ret != 0) {
        switch (ret) {
        case EDEADLK:
            OS_LOG(LOG_ERROR,"the mutex is already locked by the calling thread"
                   " (``error checking'' mutexes only).");
            break;
        case EINVAL:
            OS_LOG(LOG_ERROR,"the mutex has not been properly initialized.");
            break;
        default:
            OS_LOG(LOG_ERROR,"pthread_mutex_trylock error:%s.", strerror(ret));
            break;
        }
    }
    return ret;
}
/*****************************************************************************
 * 函 数 名  : OS_mutex_unlock
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 加锁
 * 输入参数  : mutex_lock_t *ptr  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_mutex_unlock(mutex_lock_t *ptr)
{
    if (!ptr) {
        return -1;
    }
    pthread_mutex_t *lock = (pthread_mutex_t *)ptr;
    int ret = pthread_mutex_unlock(lock);
    if (ret != 0) {
        switch (ret) {
        case EPERM:
            OS_LOG(LOG_ERROR,"the calling thread does not own the mutex"
                   " (``error checking'' mutexes only).");
            break;
        case EINVAL:
            OS_LOG(LOG_ERROR,"the mutex has not been properly initialized.");
            break;
        default:
            OS_LOG(LOG_ERROR,"pthread_mutex_trylock error:%s.", strerror(ret));
            break;
        }
    }
    return ret;
}
/*****************************************************************************
 * 函 数 名  : OS_mutex_cond_init
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 初始化
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : mutex_cond_t
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
mutex_cond_t *OS_mutex_cond_init()
{
    pthread_cond_t *cond = (pthread_cond_t *)calloc(1, sizeof(pthread_cond_t));
    if (!cond) {
        OS_LOG(LOG_ERROR,"malloc pthread_cond_t failed:%d", errno);
        return NULL;
    }
    //never return an error code
    pthread_cond_init(cond, NULL);
    return cond;
}
/*****************************************************************************
 * 函 数 名  : OS_mutex_cond_deinit
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 销毁锁柜子
 * 输入参数  : mutex_cond_t *ptr  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void OS_mutex_cond_deinit(mutex_cond_t *ptr)
{
    if (!ptr) {
        return;
    }
    pthread_cond_t *cond = (pthread_cond_t *)ptr;
    int ret = pthread_cond_destroy(cond);
    if (ret != 0) {
        switch (ret) {
        case EBUSY:
            OS_LOG(LOG_ERROR,"some threads are currently waiting on cond.");
            break;
        default:
            OS_LOG(LOG_ERROR,"pthread_cond_destroy error:%s.", strerror(ret));
            break;
        }
    }
    free(cond);
}
/*****************************************************************************
 * 函 数 名  : OS_mutex_cond_wait
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 加锁等待
 * 输入参数  : mutex_lock_t *mutexp
               mutex_cond_t *condp   
               int64_t ms            
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_mutex_cond_wait(mutex_lock_t *mutexp, mutex_cond_t *condp, int64_t ms)
{
    if (!condp || !mutexp) {
        return -1;
    }
    int ret = 0;
    int retry = 3;
    pthread_mutex_t *mutex = (pthread_mutex_t *)mutexp;
    pthread_cond_t *cond = (pthread_cond_t *)condp;
    if (ms <= 0) {
        //never return an error code
        pthread_cond_wait(cond, mutex);
    } else {
        struct timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);
        uint64_t ns = ts.tv_sec * 1000 * 1000 * 1000 + ts.tv_nsec;
        ns += ms * 1000 * 1000;
        ts.tv_sec = ns / (1000 * 1000 * 1000);
        ts.tv_nsec = ns % 1000 * 1000 * 1000;
wait:
        ret = pthread_cond_timedwait(cond, mutex, &ts);
        if (ret != 0) {
            switch (ret) {
            case ETIMEDOUT:
                OS_LOG(LOG_ERROR,"the condition variable was not signaled "
                       "until the timeout specified by abstime.");
                break;
            case EINTR:
                OS_LOG(LOG_ERROR,"pthread_cond_timedwait was interrupted by a signal.");
                if (--retry != 0) {
                    goto wait;
                }
                break;
            default:
                OS_LOG(LOG_ERROR,"pthread_cond_timedwait error:%s.", strerror(ret));
                break;
            }
        }
    }
    return ret;
}
/*****************************************************************************
 * 函 数 名  : OS_mutex_cond_signal
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 
 * 输入参数  : mutex_cond_t *ptr 
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void OS_mutex_cond_signal(mutex_cond_t *ptr)
{
    if (!ptr) {
        return;
    }
    pthread_cond_t *cond = (pthread_cond_t *)ptr;
    //never return an error code
    pthread_cond_signal(cond);
}
/*****************************************************************************
 * 函 数 名  : OS_mutex_cond_signal_all
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 全部解锁
 * 输入参数  : mutex_cond_t *ptr  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void OS_mutex_cond_signal_all(mutex_cond_t *ptr)
{
    if (!ptr) {
        return;
    }
    pthread_cond_t *cond = (pthread_cond_t *)ptr;
    //never return an error code
    pthread_cond_broadcast(cond);
}

/******************************************************************************
 * read-write lock APIs
 *****************************************************************************/
/*****************************************************************************
 * 函 数 名  : OS_rwlock_init
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 读写锁初始化
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : rw_lock_t
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
rw_lock_t *OS_rwlock_init()
{
    pthread_rwlock_t *lock = (pthread_rwlock_t *)calloc(1,
                                               sizeof(pthread_rwlock_t));
    if (!lock) {
        OS_LOG(LOG_ERROR,"malloc pthread_rwlock_t failed:%d", errno);
        return NULL;
    }
    int ret = pthread_rwlock_init(lock, NULL);
    if (ret != 0) {
        switch (ret) {
        case EAGAIN:
            OS_LOG(LOG_ERROR,"The system lacked the necessary resources (other than"
                   " memory) to initialize another read-write lock.");
            break;
        case ENOMEM:
            OS_LOG(LOG_ERROR,"Insufficient memory exists to initialize the "
                   "read-write lock.");
            break;
        case EPERM:
            OS_LOG(LOG_ERROR,"The caller does not have the privilege to perform "
                   "the operation.");
            break;
        default:
            OS_LOG(LOG_ERROR,"pthread_rwlock_init failed:%d", ret);
            break;
        }
        free(lock);
        lock = NULL;
    }

    return lock;
}

/*****************************************************************************
 * 函 数 名  : OS_rwlock_deinit
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 读写锁销毁
 * 输入参数  : rw_lock_t *ptr  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void OS_rwlock_deinit(rw_lock_t *ptr)
{
    if (!ptr) {
        return;
    }
    pthread_rwlock_t *lock = (pthread_rwlock_t *)ptr;
    if (0 != pthread_rwlock_destroy(lock)) {
        OS_LOG(LOG_ERROR,"pthread_rwlock_destroy failed!");
    }
    free(lock);
}
/*****************************************************************************
 * 函 数 名  : OS_rwlock_rdlock
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 读取读写锁
 * 输入参数  : rw_lock_t *ptr  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_rwlock_rdlock(rw_lock_t *ptr)
{
    if (!ptr) {
        return -1;
    }
    pthread_rwlock_t *lock = (pthread_rwlock_t *)ptr;
    int ret = pthread_rwlock_rdlock(lock);
    if (ret != 0) {
        switch (ret) {
        case EBUSY:
            OS_LOG(LOG_ERROR,"The read-write lock could not be acquired for reading "
                   "because a writer holds the lock or a writer with the "
                   "appropriate priority was blocked on it.");
            break;
        case EAGAIN:
            OS_LOG(LOG_ERROR,"The read lock could not be acquired because the maximum "
                   "number of read locks for rwlock has been exceeded.");
            break;
        case EDEADLK:
            OS_LOG(LOG_ERROR,"A deadlock condition was detected or the current thread "
                   "already owns the read-write lock for writing.");
            break;
        default:
            OS_LOG(LOG_ERROR,"pthread_rwlock_destroy failed!");
            break;
        }
    }
    return ret;
}
/*****************************************************************************
 * 函 数 名  : OS_rwlock_tryrdlock
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 读取非阻塞读写锁中的锁
 * 输入参数  : rw_lock_t *ptr  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_rwlock_tryrdlock(rw_lock_t *ptr)
{
    if (!ptr) {
        return -1;
    }
    pthread_rwlock_t *lock = (pthread_rwlock_t *)ptr;
    int ret = pthread_rwlock_tryrdlock(lock);
    if (ret != 0) {
        switch (ret) {
        case EBUSY:
            OS_LOG(LOG_ERROR,"The read-write lock could not be acquired for reading "
                   "because a writer holds the lock or a writer with the "
                   "appropriate priority was blocked on it.");
            break;
        case EAGAIN:
            OS_LOG(LOG_ERROR,"The read lock could not be acquired because the maximum "
                   "number of read locks for rwlock has been exceeded.");
            break;
        case EDEADLK:
            OS_LOG(LOG_ERROR,"A deadlock condition was detected or the current thread "
                   "already owns the read-write lock for writing.");
            break;
        default:
            OS_LOG(LOG_ERROR,"pthread_rwlock_tryrdlock failed!");
            break;
        }
    }
    return ret;
}
/*****************************************************************************
 * 函 数 名  : OS_rwlock_wrlock
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 写入读写锁中的锁
 * 输入参数  : rw_lock_t *ptr  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_rwlock_wrlock(rw_lock_t *ptr)
{
    if (!ptr) {
        return -1;
    }
    pthread_rwlock_t *lock = (pthread_rwlock_t *)ptr;
    int ret = pthread_rwlock_wrlock(lock);
    if (ret != 0) {
        switch (ret) {
        case EDEADLK:
            OS_LOG(LOG_ERROR,"A deadlock condition was detected or the current thread "
                  "already owns the read-write lock for writing or reading.");
            break;
        default:
            OS_LOG(LOG_ERROR,"pthread_rwlock_wrlock failed!");
            break;
        }
    }
    return ret;
}
/*****************************************************************************
 * 函 数 名  : OS_rwlock_trywrlock
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 写入非阻塞读写锁中的锁
 * 输入参数  : rw_lock_t *ptr  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_rwlock_trywrlock(rw_lock_t *ptr)
{
    if (!ptr) {
        return -1;
    }
    pthread_rwlock_t *lock = (pthread_rwlock_t *)ptr;
    int ret = pthread_rwlock_trywrlock(lock);
    if (ret != 0) {
        switch (ret) {
        case EBUSY:
            OS_LOG(LOG_ERROR,"The read-write lock could not be acquired for writing "
                   "because it was already locked for reading or writing.");
            break;

        default:
            OS_LOG(LOG_ERROR,"pthread_rwlock_trywrlock failed!");
            break;
        }
    }
    return ret;
}


/*****************************************************************************
 * 函 数 名  : OS_rwlock_unlock
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 解除锁定读写锁
 * 输入参数  : rw_lock_t *ptr  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_rwlock_unlock(rw_lock_t *ptr)
{
    if (!ptr) {
        return -1;
    }
    pthread_rwlock_t *lock = (pthread_rwlock_t *)ptr;
    int ret = pthread_rwlock_unlock(lock);
    if (ret != 0) {
        switch (ret) {
        default:
            OS_LOG(LOG_ERROR,"pthread_rwlock_unlock failed!");
            break;
        }
    }
    return ret;
}

/******************************************************************************
 * sem lock APIs
 *****************************************************************************/
/*****************************************************************************
 * 函 数 名  : OS_sem_lock_init
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 信号锁初始化
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : sem_lock_t
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
sem_lock_t *OS_sem_lock_init()
{
    sem_t *lock = (sem_t *)calloc(1, sizeof(sem_t));
    if (!lock) {
        OS_LOG(LOG_ERROR,"malloc sem_t failed:%d", errno);
        return NULL;
    }
    int pshared = 0;//0: threads, 1: processes
    if (0 != sem_init(lock, pshared, 0)) {
        OS_LOG(LOG_ERROR,"sem_init failed %d:%s", errno, strerror(errno));
        free(lock);
        lock = NULL;
    }
    return lock;
}
/*****************************************************************************
 * 函 数 名  : OS_sem_lock_deinit
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 信号锁销毁
 * 输入参数  : sem_lock_t *ptr  锁柜子
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void OS_sem_lock_deinit(sem_lock_t *ptr)
{
    if (!ptr) {
        return;
    }
    sem_t *lock = (sem_t *)ptr;
    if (0 != sem_destroy(lock)) {
        OS_LOG(LOG_ERROR,"sem_destroy %d:%s", errno , strerror(errno));
    }
    free(lock);
}
/*****************************************************************************
 * 函 数 名  : OS_sem_lock_wait
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 信号锁占用指定时间
 * 输入参数  : sem_lock_t *ptr  锁柜子
               int64_t ms       时间
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_sem_lock_wait(sem_lock_t *ptr, int64_t ms)
{
    if (!ptr) {
        return -1;
    }
    int ret;
    sem_t *lock = (sem_t *)ptr;
    if (ms < 0) {
        ret = sem_wait(lock);
        if (ret != 0) {
            switch (errno) {
            case EINTR:
                OS_LOG(LOG_ERROR,"The call was interrupted by a signal handler.");
                break;
            case EINVAL:
                OS_LOG(LOG_ERROR,"sem is not a valid semaphore.");
                break;
            }
        }
    } else {
        struct timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);
        uint64_t ns = ts.tv_sec * 1000 * 1000 * 1000 + ts.tv_nsec;
        ns += ms * 1000 * 1000;
        ts.tv_sec = ns / (1000 * 1000 * 1000);
        ts.tv_nsec = ns % 1000 * 1000 * 1000;
        ret = sem_timedwait(lock, &ts);
        if (ret != 0) {
            switch (errno) {
            case EINVAL:
                OS_LOG(LOG_ERROR,"The value of abs_timeout.tv_nsecs is less than 0, "
                       "or greater than or equal to 1000 million.");
                break;
            case ETIMEDOUT:
                OS_LOG(LOG_ERROR,"The call timed out before the semaphore could be locked.");
                break;
            }
        }
    }
    return ret;
}
/*****************************************************************************
 * 函 数 名  : OS_sem_lock_trywait
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 信号锁尝试等待获取
 * 输入参数  : sem_lock_t *ptr 
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_sem_lock_trywait(sem_lock_t *ptr)
{
    if (!ptr) {
        return -1;
    }
    int ret;
    sem_t *lock = (sem_t *)ptr;
    ret = sem_trywait(lock);
    if (ret != 0) {
        switch (errno) {
        case EAGAIN:
            OS_LOG(LOG_ERROR,"The operation could not be performed without blocking");
            break;
        }
    }
    return ret;
}
/*****************************************************************************
 * 函 数 名  : OS_sem_lock_signal
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年7月8日
 * 函数功能  : 解锁信号锁来自信号
 * 输入参数  : sem_lock_t *ptr  
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int OS_sem_lock_signal(sem_lock_t *ptr)
{
    if (!ptr) {
        return -1;
    }
    int ret;
    sem_t *lock = (sem_t *)ptr;
    ret = sem_post(lock);
    if (ret != 0) {
        switch (errno) {
        case EINVAL:
            OS_LOG(LOG_ERROR,"sem is not a valid semaphore.");
            break;
        case EOVERFLOW:
            OS_LOG(LOG_ERROR,"The maximum allowable value for a semaphore would be exceeded.");
            break;
        }
    }
    return ret;
}

