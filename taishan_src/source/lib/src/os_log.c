/***********************************************************************************
 * 文 件 名   : os_log.c
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年4月1日
 * 文件描述   : 模块间的日志公用接口文件
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   :
 * 修改日志   :
***********************************************************************************/
#include<unistd.h>
#include <pthread.h>
#include <sys/stat.h>
#include "os_log.h"

#define MAXLEVELNUM (3)

static pthread_mutex_t gMutex = PTHREAD_MUTEX_INITIALIZER;

static LOG  Debug_LogConf;
static char Debug_Logfile[] = "/var/log/message";
static modecfg   gDebugModeCfgInfo[MAX_MODE_ALL_NUMBER + 1] = {0};//global debug mode configer
const static char  Debug_LogLevelText[][10] = {"ERROR", "WARN", "NOTE", "INFO", "DEBUG"};


/*****************************************************************************
 * 函 数 名  : Debug_GetDateStr
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 获取年月日格式字符串
 * 输入参数  : char *date  输出日期字符串
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
int Debug_GetDateStr(char *datebuf)
{
    time_t timer = time(NULL);
    struct tm now_time;

    OS_ASSERT_PTR(datebuf, RET_ERR);
    strftime(datebuf, 11, "%Y-%m-%d", localtime_r(&timer, &now_time));

    return RET_OK;
}

/*****************************************************************************
 * 函 数 名  : Debug_SetTimeStr
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 获取当前的系统时间
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
static void Debug_SetTimeStr()
{
    time_t timer = time(NULL);
    struct tm now_time;
    strftime(Debug_LogConf.Current_LogTime, 20, "%Y-%m-%d %H:%M:%S", localtime_r(&timer, &now_time));
}

/*****************************************************************************
 * 函 数 名  : Debug_PrintfLogs
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 打印输出日志信息到文本
 * 输入参数  : int iModePid            模块id
               unsigned char loglevel  日志等级
               const char *pFunction   打印函数
               unsigned int uLine      当前调用行
               const char *pFileName   函数所在文件
               char * fromat           数据输出格式
               va_list args            不定参数列表
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
static void Debug_PrintfLogs(int iModePid, unsigned char loglevel, const char *pFunction, unsigned int uLine,
                             const char *pFileName, char * fromat, va_list args)
{
    char *pSrcFlie = NULL;

    OS_ASSERT_PTR_RET_VOID_AND_NO_LOG(pFunction);
    OS_ASSERT_PTR_RET_VOID_AND_NO_LOG(pFileName);
    OS_ASSERT_PTR_RET_VOID_AND_NO_LOG(fromat);
    OS_ASSERT_PTR_RET_VOID_AND_NO_LOG(Debug_LogConf.iLogFileFD);

    //组装日志时间
    Debug_SetTimeStr();
    //写入日期与日志等级到文件
    fprintf(Debug_LogConf.iLogFileFD, "[%s] [%s] ",
            Debug_LogConf.Current_LogTime, Debug_LogLevelText[loglevel]);
    vfprintf (Debug_LogConf.iLogFileFD, fromat, args);

    pSrcFlie = strrchr(pFileName, '/');
    if(NULL != pSrcFlie)
        pSrcFlie++;

    fprintf(Debug_LogConf.iLogFileFD, " [%s][%s,%d][%s]\n",
            gDebugModeCfgInfo[iModePid].sModeName, pFunction, uLine, (NULL == pSrcFlie ? pFileName : pSrcFlie));

    //同步io写入到文件
    fflush(Debug_LogConf.iLogFileFD);

}

/*****************************************************************************
 * 函 数 名  : CompressLogFilePack
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月4日
 * 函数功能  : 当日志文件达到限制后打包为tgz文件
 * 输入参数  : void  无参
 * 输出参数  : 无
 * 返 回 值  : static u32
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
static s32 CompressLogFilePack( void )
{
    struct stat statbuff; ;
    struct tm now_time;
    time_t timer = time(NULL);
    char datetime[MAX_DATE_STR_LEN] = {0};
    char pCmdBuf[MAX_CMD_BUFFER_LEN] = {0};
    char LogFilePath[MAX_FILE_PATH_LEN] = {0};
    char *pPos = NULL;

    if(stat(Debug_LogConf.Log_FilePath, &statbuff) < 0)
    {

        if(0 > access(Debug_LogConf.Log_FilePath, F_OK))
        {
            snprintf(pCmdBuf, MAX_CMD_BUFFER_LEN - 1, "touch %s", Debug_LogConf.Log_FilePath);
            system(pCmdBuf);
            Debug_LogConf.iLogFileFD = fopen(Debug_LogConf.Log_FilePath, "a+");
        }
        return RET_ERR;
    }

    if(MB_TO_BYTE(Debug_LogConf.iBuffMAxSize) < statbuff.st_size)
    {
        /* 问 题 单: 0     修改人:卢美宏,   时间:2018-04-12
           修改原因: 文件句柄不为空先关闭文件再操作避免句柄泄露 */
        if(NULL != Debug_LogConf.iLogFileFD)
        {
            (void)fclose(Debug_LogConf.iLogFileFD);
            Debug_LogConf.iLogFileFD = NULL;
        }
        //获取时间字符串
        strftime(datetime, MAX_DATE_STR_LEN, "%Y%m%d%H%M%S", localtime_r(&timer, &now_time));
        //移动文件重命名
        snprintf(pCmdBuf, MAX_CMD_BUFFER_LEN - 1, "mv %s %s_%s 2> /dev/null", Debug_LogConf.Log_FilePath, Debug_LogConf.Log_FilePath, datetime);
        system(pCmdBuf);
        strncpy(LogFilePath, Debug_LogConf.Log_FilePath, MAX_FILE_PATH_LEN - 1);
        pPos = strrchr(LogFilePath, '/');
        LogFilePath[pPos - LogFilePath] = ' ';
        LogFilePath[MAX_FILE_PATH_LEN - 1] = '\0';

        //准备打包tgz文件
        snprintf(pCmdBuf, MAX_CMD_BUFFER_LEN - 1 , "tar -czf %s_%s.tgz -C %s_%s 2> /dev/null",
                 Debug_LogConf.Log_FilePath, datetime, LogFilePath, datetime);
        system(pCmdBuf);

        //移除message文件
        snprintf(pCmdBuf, MAX_CMD_BUFFER_LEN - 1, "rm -rf %s_%s 2> /dev/null", Debug_LogConf.Log_FilePath, datetime);
        system(pCmdBuf);
    }
    //打开文件
    if(NULL == Debug_LogConf.iLogFileFD)
    {
        Debug_LogConf.iLogFileFD = fopen(Debug_LogConf.Log_FilePath, "a+");
    }
    return RET_OK;
}


/*****************************************************************************
 * 函 数 名  : Debug_InitLog
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 初始化对应模块的日志等级信息
 * 输入参数  :  __attribute__((constructor)) 在main() 之前执行,__attribute__((destructor)) 在main()执行结束之后执行.
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
__attribute__((constructor)) void Debug_InitLog(void)
{

    char bufcmd[MAX_BUFFER_LEN] = {0};


    //排队互斥锁,文件大小限制

    pthread_mutex_init(&gMutex, NULL); //初始化锁
    gDebugModeCfgInfo[MAX_MODE_ALL_NUMBER].iLogLevel = LOG_INFO;//未知模块的日志打印级别
    Debug_LogConf.iBuffMAxSize = MAX_BUFFER_MANAGE_SIZE;//文件最大限制大小，超过就打包

    if(0 >= strlen(Debug_LogConf.Log_FilePath))
    {
        memcpy(Debug_LogConf.Log_FilePath, Debug_Logfile, MAX_FILE_PATH_LEN - 1);
        Debug_LogConf.Log_FilePath[MAX_FILE_PATH_LEN - 1] = '\0';
    }

    if(0 > access(Debug_LogConf.Log_FilePath, F_OK))
    {
        snprintf(bufcmd, MAX_BUFFER_LEN - 1 , "touch %s", Debug_LogConf.Log_FilePath);
        system(bufcmd);
        Debug_LogConf.iLogFileFD = NULL;
    }

    //打开文件
    if(NULL == Debug_LogConf.iLogFileFD)
    {
        Debug_LogConf.iLogFileFD = fopen(Debug_LogConf.Log_FilePath, "a+");
    }

}

/*****************************************************************************
 * 函 数 名  : Debug_Log
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 对外写日志接口
 * 输入参数  : int iModePid            模块的id
               unsigned char loglevel  日志等级
               const char *pFunction   调用日志接口函数
               int uLine               调用行
               const char *pFileName   调用函数所在文件
               char *fromat            输出格式
               ...                     不定参数
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
int Debug_Log(int iModePid, unsigned char loglevel, const char *pFunction, int uLine, const char *pFileName, char *fromat, ...)
{
    va_list args;

    /* 问 题 单: -     修改人:卢美宏,   时间:2018-04-13
       修改原因: 日志等级判断逻辑错误 */
    if((LOG_LIMIT >= loglevel || LOG_BUTT <= loglevel) || (loglevel > gDebugModeCfgInfo[iModePid].iLogLevel))
    {
        return RET_ERR;
    }

    pthread_mutex_lock(&gMutex);  //mutex加锁
    //日志是否需要打包
    CompressLogFilePack();
    //封装日志信息体
    va_start(args, fromat);
    Debug_PrintfLogs(iModePid, loglevel, pFunction, uLine, pFileName, fromat, args);
    va_end(args);
    pthread_mutex_unlock(&gMutex);  //mutex解锁



    return RET_OK;
}

/*****************************************************************************
 * 函 数 名  : Debug_InitLogFromMode
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 模块的日志信息初始化
 * 输入参数  : int iModePid             模块的id
               const char *pModeName    模块的名字
               unsigned char iLoglevel  日志等级
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
int Debug_InitLogFromMode(int iModePid, const char *pModeName, unsigned char iLoglevel)
{
    OS_ASSERT_PTR(pModeName, RET_ERR);

    if((0 <= iModePid) && (iModePid < MAX_MODE_ALL_NUMBER))
    {
        gDebugModeCfgInfo[iModePid].iLogLevel = iLoglevel;
    }
    else
    {
        return RET_ERR;
    }
    //入参已校验
    strncpy(gDebugModeCfgInfo[iModePid].sModeName, pModeName, MAX_MODE_NAME_LEN - 1);
    gDebugModeCfgInfo[iModePid].sModeName[MAX_MODE_NAME_LEN - 1] = '\0';

    return RET_OK;
}
/*****************************************************************************
 * 函 数 名  : Debug_ChangeLogLevelFromMode
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 修改指定模块的日志等级信息
 * 输入参数  : int iModePid             模块的id
               unsigned char iLoglevel  日志等级
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
int Debug_ChangeLogLevelFromMode(int iModePid, unsigned char iLoglevel)
{
    if((0 <= iModePid) && (iModePid < MAX_MODE_ALL_NUMBER)
       && (LOG_LIMIT >= iLoglevel || LOG_BUTT <= iLoglevel))
    {
        gDebugModeCfgInfo[iModePid].iLogLevel = iLoglevel;
        return RET_OK;
    }
    else
    {
        return RET_ERR;
    }
}


