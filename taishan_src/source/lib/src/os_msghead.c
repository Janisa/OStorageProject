/***********************************************************************************
 * 文 件 名   : os_msghead.c
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年5月11日
 * 文件描述   : 消息处理功能
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   :
 * 修改日志   :
***********************************************************************************/
#include "os_msghead.h"
#include<event.h>

static char g_MsgEncryptFormatBuffer[MSG_FRMAT_SZIE] = {0};

/*****************************************************************************
 * 函 数 名  : Os_EncryptMsg
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年5月11日
 * 函数功能  : 封装消息到缓存区
 * 输入参数  : char *pBuffer  数据缓存区
               MSGHEAD *pmsg  数据头信息
               char *pData    消息数据
               s32 buflen     消息数据大小
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
s32 Os_EncryptMsg(char *pBuffer, MSGHEAD *pmsg, char *pData, s32 buflen)
{

    return snprintf(pBuffer, buflen + MSGHAED_STR_SIZE, g_MsgEncryptFormatBuffer,
                    pmsg->uiSendPid,
                    pmsg->uiRecvPid,
                    pmsg->uiSendNode,
                    pmsg->uiRecvNode,
                    pmsg->uiOperCode,
                    pmsg->uiRquesFun,
                    pmsg->uiMsgBuffLen,
                    pData);
}

/*****************************************************************************
 * 函 数 名  : os_MsgFormat
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年5月12日
 * 函数功能  : 在main函数前初始化好消息封装格式
 * 输入参数  : void
 * 输出参数  : 无
 * 返 回 值  : __attribute__((constructor)) 在main() 之前执行,__attribute__((destructor)) 在main()执行结束之后执行.
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
__attribute__((constructor)) void Os_MsgFormatInitial(void)
{
    snprintf(g_MsgEncryptFormatBuffer, MSG_FRMAT_SZIE - 1, "%%0%dx%%0%dx%%0%dx%%0%dx%%0%dx%%0%dx%%0%dx%%s",
             uiSendNodeLen,
             uiRecvPidLen,
             uiSendNodeLen,
             uiRecvNodeLen,
             uiOperCodeLen,
             uiRquesFunLen,
             uiMsgBufferLen);
}

/*****************************************************************************
 * 函 数 名  : HexStrToInt
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年5月12日
 * 函数功能  : 十六进制字符串装十进制整数
 * 输入参数  : char* str  待转换的十六进制字符串
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
static s64 Os_HexStrToInt(char* str)
{
    s64  value = 0;

    if (!str)
    {
        return 0;
    }

    while (1)
    {
        if ((*str >= '0') && (*str <= '9'))
        {
            value = value * 16 + (*str - '0');
        }
        else if ((*str >= 'A') && (*str <= 'F'))
        {
            value = value * 16 + (*str - 'A') + 10;
        }
        else if ((*str >= 'a') && (*str <= 'f'))
        {
            value = value * 16 + (*str - 'a') + 10;
        }
        else
        {
            break;
        }
        str++;
    }
    return value;
}
/*****************************************************************************
 * 函 数 名  : Os_DecodeMsg
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年5月12日
 * 函数功能  : 解码消息
 * 输入参数  : MSGHEAD * pmsg  消息头结构体
               char *pBuffer   包含消息头的数据流
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
void  Os_DecodeMsg(MSGHEAD * pmsg, char *pBuffer)
{
    MSGHEAD_STR msg_head;
    memset(&msg_head, 0, MSGHAED_STR_SIZE);

    /* 解码消息头,字符拷贝以防字节对齐 */
    strncpy(msg_head.uiSendPid, pBuffer, uiSendNodeLen);
    pmsg->uiSendPid = Os_HexStrToInt(msg_head.uiSendPid);

    strncpy(msg_head.uiRecvPid,  pBuffer + uiSendNodeLen, uiRecvPidLen);
    pmsg->uiRecvPid = Os_HexStrToInt(msg_head.uiRecvPid);

    strncpy(msg_head.uiSendNode, pBuffer + uiSendNodeLen + uiRecvPidLen, uiSendNodeLen);
    pmsg->uiSendNode = Os_HexStrToInt(msg_head.uiSendNode);

    strncpy(msg_head.uiRecvNode, pBuffer + uiSendNodeLen + uiRecvPidLen + uiSendNodeLen, uiRecvNodeLen);
    pmsg->uiRecvNode = Os_HexStrToInt(msg_head.uiRecvNode);

    strncpy(msg_head.uiOperCode, pBuffer + uiSendNodeLen + uiRecvPidLen + uiSendNodeLen + uiRecvNodeLen, uiOperCodeLen);
    pmsg->uiOperCode = Os_HexStrToInt(msg_head.uiOperCode);

    strncpy(msg_head.uiRquesFun, pBuffer + uiSendNodeLen + uiRecvPidLen + uiSendNodeLen + uiRecvNodeLen + uiOperCodeLen, uiRquesFunLen);
    pmsg->uiRquesFun = Os_HexStrToInt(msg_head.uiRquesFun);

    strncpy(msg_head.uiMsgBuffLen, pBuffer + uiSendNodeLen + uiRecvPidLen + uiSendNodeLen + uiRecvNodeLen + uiOperCodeLen + uiRquesFunLen, uiMsgBufferLen);
    pmsg->uiMsgBuffLen = Os_HexStrToInt(msg_head.uiMsgBuffLen);

    pmsg->RequesData = (pBuffer + MSGHAED_STR_SIZE);
}

