/***********************************************************************************
 * 文 件 名   : os_initmodel.c
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年6月23日
 * 文件描述   : 构建初始化函数表模块
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   : 
 * 修改日志   : 
***********************************************************************************/

#include "os_initmodel.h"

/*
 * These two variables are defined in link script.
 */
extern init_call _init_start;
extern init_call _init_end;
 

/*****************************************************************************
 * 函 数 名  : A_init
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年6月23日
 * 函数功能  : 构建初始化函数表使用例子
 * 输入参数  : void 
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void A_init()
{
    OS_LOG(LOG_ERROR,"A_init\n");
}
//DECLARE_INIT(A_init);
/*****************************************************************************
 * 函 数 名  : Os_initcalls
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年6月23日
 * 函数功能  : 初始化入口，遍历函数表调用
 * 输入参数  : void  
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void Os_initcalls()
{
    init_call *init_ptr = &_init_start;
    for (; init_ptr < &_init_end; init_ptr++) {
        OS_LOG(LOG_ERROR,"init address: %p\n", init_ptr);
        (*init_ptr)();
    }
}
#if 0
int main(void)
{
    Os_initcalls();
    return 0;
}
#endif

