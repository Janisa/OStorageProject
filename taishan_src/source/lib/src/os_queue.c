/***********************************************************************************
 * 文 件 名   : os_queue.c
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年4月1日
 * 文件描述   : 队列操作函数接口封装
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   :
 * 修改日志   :
***********************************************************************************/

#include<malloc.h>
#include"os_queue.h"
#include "os_defines.h"

#define QUEUEIVABLE(pqueue,pnode) (OS_IsEmpty(pqueue) != true && NULL != pnode)


/*****************************************************************************
 * 函 数 名  : OS_InitQueue
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 构造一个空队列
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : Queue
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
Queue *OS_InitQueue()
{
    Queue *pqueue = (Queue *)malloc(sizeof(Queue));

    if(pqueue == NULL)
    {
        return NULL;
    }
    pqueue->front = NULL;
    pqueue->rear = NULL;
    pqueue->size = 0;
    return pqueue;
}

/*****************************************************************************
 * 函 数 名  : OS_DestroyQueue
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 销毁一个队列
 * 输入参数  : Queue *pqueue  队列指针
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
void OS_DestroyQueue(Queue *pQueue)
{
    OS_ASSERT_PTR_RET_VOID_AND_NO_LOG(pQueue);
    if(OS_IsEmpty(pQueue) != true)
        OS_ClearQueue(pQueue);
    free(pQueue);
}

/*****************************************************************************
 * 函 数 名  : OS_ClearQueue
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 清空一个队列
 * 输入参数  : Queue *pqueue  队列指针
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
void OS_ClearQueue(Queue *pQueue)
{
    OS_ASSERT_PTR_RET_VOID(pQueue);
    while(OS_IsEmpty(pQueue) != true)
    {
        OS_DeQueue(pQueue, NULL);
    }

}


/*****************************************************************************
 * 函 数 名  : OS_IsEmpty
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 判断队列是否为空
 * 输入参数  : Queue *pqueue  队列指针
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
bool OS_IsEmpty(Queue *pQueue)
{
    OS_ASSERT_PTR_RET_NO_LOG(pQueue, false);
    if(NULL == pQueue->front
       && NULL == pQueue->rear
       && 0 == pQueue->size )
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*****************************************************************************
 * 函 数 名  : OS_GetSize
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 返回队列大小
 * 输入参数  : Queue *pqueue  队列指针
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
int OS_GetSize(Queue *pQueue)
{
    OS_ASSERT_PTR_RET_NO_LOG(pQueue, 0);
    return pQueue->size;
}

/*****************************************************************************
 * 函 数 名  : OS_GetFront
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 返回队列头元素
 * 输入参数  : Queue *pqueue  队列指针
               Item *pitem    出队元素
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
PNode OS_GetFront(Queue *pQueue, Item *pitem)
{
    OS_ASSERT_PTR_RET_NO_LOG(pQueue, NULL);
    if(QUEUEIVABLE(pQueue, pitem))
    {
        *pitem = pQueue->front->data;
    }
    return pQueue->front;
}

/*****************************************************************************
 * 函 数 名  : OS_GetRear
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 返回队列尾元素
 * 输入参数  : Queue *pqueue  队列指针
               Item *pitem    出队元素
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
PNode OS_GetRear(Queue *pQueue, Item *pitem)
{
    OS_ASSERT_PTR_RET_NO_LOG(pQueue, NULL);
    if(QUEUEIVABLE(pQueue, pitem))
    {
        *pitem = pQueue->rear->data;
    }
    return pQueue->rear;
}

/*****************************************************************************
 * 函 数 名  : OS_EnQueue
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 元素插入队列
 * 输入参数  : Queue *pqueue  队列指针
               Item item      插入元素
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
PNode OS_EnQueue(Queue *pQueue, Item item)
{
    PNode pnode = (PNode)malloc(sizeof(Node));
    OS_ASSERT_PTR_RET_NO_LOG(pQueue, NULL);
    if(pnode != NULL)
    {
        pnode->data = item;
        pnode->next = NULL;

        if(OS_IsEmpty(pQueue))
        {
            pQueue->front = pnode;
        }
        else
        {
            pQueue->rear->next = pnode;
        }
        pQueue->rear = pnode;
        pQueue->size++;
    }
    return pnode;
}

/*****************************************************************************
 * 函 数 名  : OS_DeQueue
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 队列头元素出列
 * 输入参数  : Queue *pqueue  队列指针
               Item *pitem    出列元素
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
PNode OS_DeQueue(Queue *pQueue, Item *pitem)
{
    PNode pnode = pQueue->front;
    OS_ASSERT_PTR_RET_NO_LOG(pQueue, NULL);
    if(QUEUEIVABLE(pQueue, pitem))
    {
        if(pitem != NULL)
        {
            *pitem = pnode->data;
        }
        pQueue->size--;
        pQueue->front = pnode->next;
        free(pnode);
        if(0 == pQueue->size)
        {
            pQueue->rear = NULL;
        }
    }
    return pQueue->front;
}

/*****************************************************************************
 * 函 数 名  : OS_QueueTraverse
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 遍历队列并对各数据项调用visit函数
 * 输入参数  : Queue *pqueue    队列指针
               void (*visit)()  执行函数
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
void OS_QueueTraverse(Queue *pQueue, void (*visit)())
{
    PNode pnode = pQueue->front;
    OS_ASSERT_PTR_RET_VOID_AND_NO_LOG(pQueue);
    int i = pQueue->size;
    while(i--)
    {
        visit(pnode->data);
        pnode = pnode->next;
    }
}

