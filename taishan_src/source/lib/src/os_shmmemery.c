/***********************************************************************************
 * 文 件 名   : os_shmmemery.c
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年4月1日
 * 文件描述   : 共享内存的操作系统接通封装
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   :
 * 修改日志   :
***********************************************************************************/

#include "os_shmmemery.h"
#include "os_log.h"


#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <sys/shm.h>

/*****************************************************************************
 * 函 数 名  : OS_ShmMem_Initial
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 初始化指定的共享内存，存在直接打不开，否则新建，同时成功后悔
               拿到对方的pid，建议放在子线程去完成
 * 输入参数  : const char *pathname       一个系统能访问的路径
               int proj_id                1~255之间的值
               SHMINFO *pShminfo          共享内存结构体信息指针
               void (*sighandler_t)(int)  signal用户信号毁掉函数
               int bufflen                共享内存建立大小
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
s32 OS_ShmMem_Initial(const char *pathname, int proj_id, SHMINFO *pShminfo, void (*sighandler_t)(int), int bufflen)
{
    key_t key;
    OS_ASSERT_PTR_RET_NO_LOG(pShminfo, RET_ERR);
    OS_ASSERT_PTR_RET_NO_LOG(pathname, RET_ERR);

    if ((key = ftok(pathname, proj_id)) < 0)
    {
        return RET_ERR;
    }

    signal(SIGUSR1, sighandler_t);//  注冊一个信号处理函数
    if ((pShminfo->iShmid = shmget(key, bufflen, 0666 | IPC_CREAT | IPC_EXCL)) < 0)
    {
        if (EEXIST == errno) //  存在则直接打开
        {
            pShminfo->iShmid = shmget(key, bufflen, 0666);
            pShminfo->pShmBufAddr = shmat(pShminfo->iShmid, NULL, 0);//得到共享内存地址
            pShminfo->iPid = atoi(pShminfo->pShmBufAddr);
            sprintf(pShminfo->pShmBufAddr, "%d", getpid());
            pShminfo->isReady = 1;
            kill(pShminfo->iPid, SIGUSR1);
            return RET_OK;
        }
        else//出错
        {
            return RET_ERR;
        }
    }
    else//成功
    {

        pShminfo->pShmBufAddr = shmat(pShminfo->iShmid, NULL, 0);//得到共享内存地址
        sprintf(pShminfo->pShmBufAddr, "%d", getpid()); //  把自己的pid写到共享内存
        pause();
        pShminfo->iPid = atoi(pShminfo->pShmBufAddr);//  得到对端进程的pid
        pShminfo->isReady = 1;
        return RET_OK;
    }


}
/*****************************************************************************
 * 函 数 名  : OS_ShmMem_ReadBuffer
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 从共享内存中读取指定大小的数据
 * 输入参数  : SHMINFO *acPShmBuffInfo  共享内存结构体指针
               char *acPOutBuff         存放共享内存中读出来的数据缓存区地址
               u32 uiMaxBuffLen         最大读取的缓存区大小
               bool iSendSignal         操作完成后是否给对方发送信号
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
s32 OS_ShmMem_ReadBuffer(SHMINFO *acPShmBuffInfo, char *acPOutBuff, u32 uiMaxBuffLen, bool iSendSignal)
{
    OS_ASSERT_PTR_RET_NO_LOG(acPShmBuffInfo, RET_ERR);
    OS_ASSERT_PTR_RET_NO_LOG(acPShmBuffInfo->pShmBufAddr, RET_ERR);
    OS_ASSERT_PTR_RET_NO_LOG(acPOutBuff, RET_ERR);
    if(memcpy(acPShmBuffInfo->pShmBufAddr, acPOutBuff, uiMaxBuffLen))
    {
        if(iSendSignal)
            kill(acPShmBuffInfo->iPid, SIGUSR1);
        return RET_OK;
    }
    return RET_ERR;
}
/*****************************************************************************
 * 函 数 名  : OS_ShmMem_WriteBuffer
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 写入指定大小数据到共享内存中去
 * 输入参数  : SHMINFO *acPShmBuffInfo  共享内存结构体指针
               char *acPInputBuff         存放待写入数据缓存区地址
               u32 uiMaxBuffLen         最大写入的缓存区大小
               bool iSendSignal         操作完成后是否给对方发送信号
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/

s32 OS_ShmMem_WriteBuffer(SHMINFO *acPShmBuffInfo, char *acPInputBuff, u32 uiMaxBuffLen, bool iSendSignal)
{
    OS_ASSERT_PTR_RET_NO_LOG(acPShmBuffInfo, RET_ERR);
    OS_ASSERT_PTR_RET_NO_LOG(acPShmBuffInfo->pShmBufAddr, RET_ERR);
    OS_ASSERT_PTR_RET_NO_LOG(acPInputBuff, RET_ERR);
    if(memcpy(acPShmBuffInfo->pShmBufAddr, acPInputBuff, uiMaxBuffLen))
    {
        if(iSendSignal)
            kill(acPShmBuffInfo->iPid, SIGUSR1);
        return RET_OK;
    }
    return RET_ERR;
}
/*****************************************************************************
 * 函 数 名  : OS_ShmMem_Destroy
 * 负 责 人  : 卢美宏
 * 创建日期  : 2018年4月1日
 * 函数功能  : 销毁共享内存
 * 输入参数  : SHMINFO *acPShmBuffInfo  共享内存信息结构体指针
 * 输出参数  : 无
 * 返 回 值  :
 * 调用关系  :
 * 其    它  :

*****************************************************************************/
s32 OS_ShmMem_Destroy(SHMINFO *acPShmBuffInfo)
{
    OS_ASSERT_PTR_RET_NO_LOG(acPShmBuffInfo, RET_ERR);
    OS_ASSERT_PTR_RET_NO_LOG(acPShmBuffInfo->pShmBufAddr, RET_ERR);
    shmdt(acPShmBuffInfo->pShmBufAddr);
    shmctl(acPShmBuffInfo->iShmid, IPC_RMID, NULL);          //  删除共享内存
    acPShmBuffInfo->pShmBufAddr = NULL;
    return RET_OK;
}

