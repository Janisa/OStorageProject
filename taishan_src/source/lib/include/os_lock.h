/***********************************************************************************
 * 文 件 名   : os_lock.h
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年7月8日
 * 文件描述   : 资源并发管理
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   : 
 * 修改日志   : 
***********************************************************************************/

#ifndef LIBLOCK_H
#define LIBLOCK_H

#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif

/*
 * spin lock implemented by atomic APIs
 */
typedef int spin_lock_t;
spin_lock_t *OS_spin_lock_init();
int OS_spin_lock(spin_lock_t *lock);
int OS_spin_unlock(spin_lock_t *lock);
int OS_spin_trylock(spin_lock_t *lock);
void OS_spin_lock_deinit(spin_lock_t *lock);


/*
 * mutex lock implemented by pthread_mutex APIs
 */
typedef void mutex_lock_t;
mutex_lock_t *OS_mutex_lock_init();
int OS_mutex_trylock(mutex_lock_t *lock);
int OS_mutex_lock(mutex_lock_t *lock);
int OS_mutex_unlock(mutex_lock_t *lock);
void OS_mutex_lock_deinit(mutex_lock_t *lock);


/*
 * external APIs of mutex condition
 */
typedef void mutex_cond_t;
mutex_cond_t *OS_mutex_cond_init();
int OS_mutex_cond_wait(mutex_lock_t *mutex, mutex_cond_t *cond, int64_t ms);
void OS_mutex_cond_signal(mutex_cond_t *cond);
void OS_mutex_cond_signal_all(mutex_cond_t *cond);
void OS_mutex_cond_deinit(mutex_cond_t *cond);


/*
 * read-write lock implemented by pthread_rwlock APIs
 */
typedef void rw_lock_t;
rw_lock_t *OS_rwlock_init();
int OS_rwlock_rdlock(rw_lock_t *lock);
int OS_rwlock_tryrdlock(rw_lock_t *lock);
int OS_rwlock_wrlock(rw_lock_t *lock);
int OS_rwlock_trywrlock(rw_lock_t *lock);
int OS_rwlock_unlock(rw_lock_t *lock);
void OS_rwlock_deinit(rw_lock_t *lock);


/*
 * sem lock implemented by Unnamed semaphores (memory-based semaphores) APIs
 */
typedef void sem_lock_t;
sem_lock_t *OS_sem_lock_init();
int OS_sem_lock_wait(sem_lock_t *lock, int64_t ms);
int OS_sem_lock_trywait(sem_lock_t *lock);
int OS_sem_lock_signal(sem_lock_t *lock);
void OS_sem_lock_deinit(sem_lock_t *lock);


#ifdef __cplusplus
}
#endif
#endif
