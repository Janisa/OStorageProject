#ifndef __OS_THREADPOOL_H__
#define __OS_THREADPOOL_H__

typedef struct threadpool_t threadpool_t;

/**
 * @function OS_Threadpool_create
 * @descCreates a threadpool_t object.
 * @param thr_num  thread num
 * @param max_thr_num  max thread size
 * @param queue_max_size   size of the queue.
 * @return a newly created thread pool or NULL
 */
threadpool_t *OS_Threadpool_create(int min_thr_num, int max_thr_num, int queue_max_size);

/**
 * @function OS_Threadpool_add
 * @desc add a new task in the queue of a thread pool
 * @param pool     Thread pool to which add the task.
 * @param function Pointer to the function that will perform the task.
 * @param argument Argument to be passed to the function.
 * @return 0 if all goes well,else -1
 */
int OS_Threadpool_add(threadpool_t *pool, void*(*function)(char *arg), void *arg);

/**
 * @function OS_Threadpool_destroy
 * @desc Stops and destroys a thread pool.
 * @param pool  Thread pool to destroy.
 * @return 0 if destory success else -1
 */
int OS_Threadpool_destroy(threadpool_t *pool);

/**
 * @desc get the thread num
 * @pool pool threadpool
 * @return # of the thread
 */
int OS_Threadpool_all_threadnum(threadpool_t *pool);

/**
 * desc get the busy thread num
 * @param pool threadpool
 * return # of the busy thread
 */
int OS_Threadpool_busy_threadnum(threadpool_t *pool);

#endif

