/***********************************************************************************
 * 文 件 名   : os_log.h
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年4月1日
 * 文件描述   : 模块间的日志公用接口头文件
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   :
 * 修改日志   :
***********************************************************************************/

/** log.h **/

#ifndef __LOG_H__
#define __LOG_H__

#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "time.h"
#include "stdarg.h"
#include "unistd.h"
#include "os_defines.h"

#define MAX_BUFFER_LEN (2048)
#define MAX_DATE_STR_LEN (16)
#define MAX_CMD_BUFFER_LEN (578)
#define MAX_FILE_PATH_LEN (256)
#define MAX_FILE_NAME_LEN (50)
#define MAX_MODE_NAME_LEN (64)
#define MAX_MODE_ALL_NUMBER (12)
#define MAX_BUFFER_MANAGE_SIZE (50) //一个日志文件最大限制xM
#define MB_TO_BYTE(x) ((x)*1024*1024) //兆转字节
#define OS_LOG(loglevel,fmt, args...)\
            Debug_Log(MAX_MODE_ALL_NUMBER,loglevel,__func__,__LINE__,__FILE__,fmt,##args)

typedef enum
{
    LOG_LIMIT = -1,
    LOG_ERROR = 0,
    LOG_WARN  = 1,
    LOG_NOTE  = 2,
    LOG_INFO  = 3,
    LOG_DEBUG = 4,
    LOG_BUTT  = 255
} LOGLEVEL;

typedef struct _log
{
    char Current_LogTime[20];
    char Log_FilePath[MAX_FILE_PATH_LEN];
    FILE *iLogFileFD;
    int  iPrintLevel;
    int  iBuffMAxSize;
} LOG;
typedef struct _modecfg
{
    char sModeName[MAX_MODE_NAME_LEN];
    int  iLogLevel;
} modecfg;
int Debug_Log(int iModePid, unsigned char loglevel, const char *pFunction, int uLine, const char *pFileName, char *fromat, ...);
//__attribute__((format(printf, 6, 7)));
int Debug_InitLogFromMode(int iModePid, const char *pModeName, unsigned char iLoglevel);
int Debug_ChangeLogLevelFromMode(int iModePid, unsigned char iLoglevel);
int Debug_GetDateStr(char *datebuf);

/*带有超频限制的日志打印函数*/
#define OS_PRINT_LIMIT_WITH_LEVEL(loglevel,interval,burst,fmt,...)\
do{ \
    static u32 uiMissed = 0; \
    static u32 uiPrinted = 0; \
    static s64 uiLast = 0; \
    s64 uiNow = time(NULL);\
    /*如果两次打印时间大于设定的时间，打印被屏蔽的日志条数*/\
    if(uiNow >= (uiLast + (interval)))\
     {\
        if(uiPrinted > 0)\
        {\
            OS_LOG(LOG_INFO,"%d repeat logs was suppressed.",uiPrinted);\
        }\
        uiPrinted = 0;\
        uiMissed = 0;\
    }\
    uiLast = uiNow;\
    if(uiPrinted < (burst))\
    {\
        uiPrinted++;\
        Debug_Log(loglevel,__func__,__LINE__,__FILE__,fmt,__VA_ARGS__);\
    }\
    else\
    {\
        uiMissed++;\
        Debug_Log(LOG_DEBUG,__func__,__LINE__,__FILE__,fmt,__VA_ARGS__);\
    }\
}while(0)

#endif
/* __LOG_H__ */

