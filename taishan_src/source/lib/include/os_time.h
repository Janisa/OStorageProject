/***********************************************************************************
 * 文 件 名   : os_time.h
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年7月8日
 * 文件描述   : 时间处理工具
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   : 
 * 修改日志   : 
***********************************************************************************/

#ifndef LIBTIME_H
#define LIBTIME_H

#include <stdint.h>
#include <stdbool.h>
#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

struct time_info {
    uint64_t utc_msec;
    uint32_t utc;
    uint16_t year;
    uint8_t  mon;
    uint8_t  day;
    uint8_t  hour;
    uint8_t  min;
    uint8_t  sec;
    uint16_t msec;
    int8_t   timezone;
    char     str[32];
};

/*
 * accuracy second
 */
uint32_t OS_time_get_sec();
char *OS_time_get_sec_str();
char *OS_time_get_str_human(char *str, int len);
char *OS_time_get_str_human_by_utc(uint32_t utc, char *str, int len);

/*
 * accuracy milli second
 */
uint64_t OS_time_get_msec();
char *OS_time_get_msec_str(char *str, int len);
int OS_time_sleep_ms(uint64_t ms);

/*
 * accuracy micro second
 */
uint64_t OS_time_get_usec();

/*
 * accuracy nano second
 */
uint64_t OS_time_get_nsec();
uint64_t OS_time_get_nsec_bootup();

int OS_time_get_info(struct time_info *ti);
int OS_time_get_info_by_utc(uint32_t utc, struct time_info *ti);

bool OS_time_passed_sec(int sec);

#ifdef __cplusplus
}
#endif
#endif
