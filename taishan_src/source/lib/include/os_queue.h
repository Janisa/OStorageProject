/***********************************************************************************
 * 文 件 名   : os_queue.h
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年4月1日
 * 文件描述   : 队列操作函数接口封装头文件
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   :
 * 修改日志   :
***********************************************************************************/

#ifndef __OS_QUEUE_H__
#define __OS_QUEUE_H__
#include <stdbool.h>

typedef void* Item;
typedef struct node * PNode;
typedef struct node
{
    Item data;
    PNode next;
} Node;

typedef struct
{
    PNode front;
    PNode rear;
    int size;
} Queue;

/*构造一个空队列*/
Queue *OS_InitQueue();

/*销毁一个队列*/
void OS_DestroyQueue(Queue *pQueue);

/*清空一个队列*/
void OS_ClearQueue(Queue *pQueue);

/*判断队列是否为空*/

bool OS_IsEmpty(Queue *pQueue);

/*返回队列大小*/
int OS_GetSize(Queue *pQueue);

/*返回队头元素*/
PNode OS_GetFront(Queue *pQueue, Item *pitem);

/*返回队尾元素*/
PNode OS_GetRear(Queue *pQueue, Item *pitem);

/*将新元素入队*/
PNode OS_EnQueue(Queue *pQueue, Item item);

/*队头元素出队*/
PNode OS_DeQueue(Queue *pQueue, Item *pitem);

/*遍历队列并对各数据项调用visit函数*/
void OS_QueueTraverse(Queue *pQueue, void (*visit)());

#endif


