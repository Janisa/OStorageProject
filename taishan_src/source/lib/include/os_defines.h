/***********************************************************************************
 * 文 件 名   : os_defines.h
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年4月1日
 * 文件描述   : 通用定义头文件
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   :
 * 修改日志   :
***********************************************************************************/

#ifndef __OS_DEFINES_H__
#define __OS_DEFINES_H__

#include <stdbool.h>
#include "os_log.h"

#define s32 long int

#define u32 unsigned int

#define s64 long long int

#define u64 unsigned long long int

#define RET_OK 0
#define RET_ERR -1
#define RET_VOID

#define RET_EXCEPTIONAL 128//异常退出
#define RET_TIMEOUT     129//超时退出
#define RET_INDIDE_ERR  130//内部错误
#define RET_STAT_D      131//进程D状态

#define STAT_PATH_LEN 64
#define STAT_BUFFER_LEN 288
#define TV_USEC_VALUE 200
#define INVALUE_INAVLE  0xffffffffffffffffUL

/*通用断言定义宏*/
#define OS_ASSERT(expr,ret) \
do{\
    if(!(expr)) \
    {\
        OS_LOG(LOG_ERROR,"Assertion fallure("#expr") is false,return("#ret").");\
        return ret;\
    }\
  }while(0)
/*通用断言定义宏*/
#define OS_ASSERT_NO_LOG(expr,ret) \
do{\
    if(!(expr)) \
    {\
        return ret;\
    }\
  }while(0)

/*空指针断言定义宏*/
#define OS_ASSERT_PTR(expr,ret)     OS_ASSERT(expr,ret)
#define OS_ASSERT_PTR_RET_VOID(expr)  OS_ASSERT(expr,RET_VOID)
#define OS_ASSERT_PTR_RET_NO_LOG(expr,ret)  OS_ASSERT_NO_LOG(expr,ret)
#define OS_ASSERT_PTR_RET_VOID_AND_NO_LOG(expr)  OS_ASSERT_NO_LOG(expr,RET_VOID)

/*内存释放定义宏*/
#define OS_FREE_PERT(expr)\
do{\
    if(NULL != (expr))\
        free((expr));\
    (expr) = NULL;\
}while(0)

#endif
/*__OS_DEFINES_H__*/
