/***********************************************************************************
 * 文 件 名   : os_shmmemery.h
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年4月1日
 * 文件描述   : 共享内存的操作系统接通封装头文件
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   :
 * 修改日志   :
***********************************************************************************/

#ifndef __OS_SHMMEMERY_H__
#define __OS_SHMMEMERY_H__
#include "os_defines.h"

typedef void (*sighandler_t)(int);

#define SHM_BUFFER_MINI_LEN 1024

typedef struct _shminfo
{
    pid_t iPid;
    int   iShmid;
    int   isReady;
    char *pShmBufAddr;
} SHMINFO;

s32 OS_ShmMem_Initial(const char *pathname, int proj_id, SHMINFO *pShminfo, void (*sighandler_t)(int), int bufflen);


s32 OS_ShmMem_ReadBuffer(SHMINFO *acPShmBuffInfo, char *acPOutBuff, u32 uiMaxBuffLen, bool iSendSignal);

s32 OS_ShmMem_WriteBuffer(SHMINFO *acPShmBuffInfo, char *acPInputBuff, u32 uiMaxBuffLen, bool iSendSignal);

s32 OS_ShmMem_Destroy(SHMINFO *acPShmBuffInfo);


#endif
