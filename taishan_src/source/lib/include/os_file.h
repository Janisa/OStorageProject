/***********************************************************************************
 * 文 件 名   : os_file.c
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年7月8日
 * 文件描述   : 文件处理工具
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   : 
 * 修改日志   : 
***********************************************************************************/

#ifndef LIBFILE_H
#define LIBFILE_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/uio.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum file_open_mode {
    F_RDONLY,
    F_WRONLY,
    F_RDWR,
    F_CREATE,
    F_WRCLEAR,
} file_open_mode_t;

struct file_desc {
    union {
        int fd;
        FILE *fp;
    };
    char *name;
};

typedef struct file {
    struct file_desc *fd;
    const struct file_ops *ops;
    uint64_t size;
} file;

typedef struct file_info {
    struct timespec time_modify;
    struct timespec time_access;
    uint64_t size;
} file_info;

typedef struct file_systat {
    uint64_t size_total;
    uint64_t size_avail;
    uint64_t size_free;
    char fs_type_name[32];
} file_systat;

typedef struct file_ops {
    struct file_desc * (*open)(const char *path, file_open_mode_t mode);
    ssize_t (*write)(struct file_desc *fd, const void *buf, size_t count);
    ssize_t (*read)(struct file_desc *fd, void *buf, size_t count);
    off_t (*seek)(struct file_desc *fd, off_t offset, int whence);
    int (*sync)(struct file_desc *fd);
    size_t (*size)(struct file_desc *fd);
    void (*close)(struct file_desc *fd);
} file_ops_t;

typedef enum file_backend_type {
    FILE_BACKEND_IO,
    FILE_BACKEND_FIO,
} file_backend_type;

void OS_file_backend(file_backend_type type);
int OS_file_create(const char *path);
void OS_file_delete(const char *path);
bool OS_file_exist(const char *path);
struct file *OS_file_open(const char *path, file_open_mode_t mode);
void OS_file_close(struct file *file);
ssize_t OS_file_read(struct file *file, void *data, size_t size);
ssize_t OS_file_write(struct file *file, const void *data, size_t size);
ssize_t OS_file_size(struct file *file);
ssize_t OS_file_get_size(const char *path);
struct iovec *OS_file_dump(const char *path);
int OS_file_sync(struct file *file);
off_t OS_file_seek(struct file *file, off_t offset, int whence);
struct file_systat *OS_file_get_systat(const char *path);
char *OS_file_path_pwd();
char *OS_file_path_suffix(char *path);
char *OS_file_path_prefix(char *path);

int OS_file_dir_create(const char *path);
int OS_file_dir_remove(const char *path);
int OS_file_dir_remove_Ext(const char *path);
int OS_file_dir_tree(const char *path);
int OS_file_dir_size(const char *path, uint64_t *size);
int OS_file_num_in_dir(const char *path);
#ifdef __cplusplus
}
#endif
#endif
