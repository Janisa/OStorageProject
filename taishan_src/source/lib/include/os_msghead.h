/***********************************************************************************
 * 文 件 名   : os_msghead.h
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年4月1日
 * 文件描述   : 消息头结构体头文件
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   :
 * 修改日志   :
***********************************************************************************/

#ifndef __OS_MSGHEAD_H__
#define __OS_MSGHEAD_H__
#include "os_defines.h"
#include "stdarg.h"



#define MSG_DATA_LEN  1048576 //1024*1024 1M
enum MSgInfoLen
{
    uiSendPidLen    = 2 ,//发送消息节点id
    uiRecvPidLen    = 2,//接收消息模块id
    uiSendNodeLen   = 2,//发送消息节点id
    uiRecvNodeLen   = 2,//接收消息节点id
    uiOperCodeLen   = 4,//接收消息段处理函数
    uiRquesFunLen   = 4,//收到消息出来后回效应处理函数
    uiMsgBufferLen  = 4//本次消息内容大小
};

/**************************
节点信息结构体
***************************/
typedef struct _MAPNODEINFO
{
    int iPid;//模块的id
    int iFD;//通信信道
    bool isMaster;//主备标识
} MAPNIDE;

/**************************
节点通信消息结构体
***************************/
typedef struct _MSGHEAD
{
    int uiSendPid;//发送消息节点id
    int uiRecvPid;//接收消息模块id
    int uiSendNode;//发送消息节点id
    int uiRecvNode;//接收消息节点id
    u32 uiOperCode;//接收消息段处理函数
    u32 uiRquesFun;//收到消息出来后回效应处理函数
    u32 uiMsgBuffLen;//本次消息内容大小
    void *RequesData;//消息内容
} MSGHEAD;

/**************************
节点通信消息结构体
***************************/
typedef struct _MSGHEAD_STR
{
    char uiSendPid[uiSendPidLen];//发送消息节点id
    char uiRecvPid[uiRecvPidLen];//接收消息模块id
    char uiSendNode[uiSendNodeLen];//发送消息节点id
    char uiRecvNode[uiRecvNodeLen];//接收消息节点id
    char uiOperCode[uiOperCodeLen];//接收消息段处理函数
    char uiRquesFun[uiRquesFunLen];//收到消息出来后回效应处理函数
    char uiMsgBuffLen[uiMsgBufferLen];//本次消息内容大小
} MSGHEAD_STR;

/**************************
初始化节点通信消息结构体
***************************/


#define OS_MsgInit(pMsgHead) \
do{\
    pMsgHead.uiSendPid  = 0;\
    pMsgHead.uiRecvPid  = 0;\
    pMsgHead.uiSendNode = 0;\
    pMsgHead.uiRecvNode = 0;\
    pMsgHead.uiOperCode = 0;\
    pMsgHead.uiRquesFun = 0;\
    pMsgHead.RequesData = NULL;\
    pMsgHead.uiMsgBuffLen = 0;\
}while(0)

#define MSGHAED_STR_SIZE (uiSendPidLen + uiRecvPidLen  + uiSendNodeLen + uiRecvNodeLen + uiOperCodeLen + uiRquesFunLen + uiMsgBufferLen)//数据头
#define MSG_FRMAT_SZIE 256

s32 Os_EncryptMsg(char *pBuffer, MSGHEAD *pmsg, char *pData, s32 buflen);
void  Os_DecodeMsg(MSGHEAD * pmsg, char *pBuffer);


#endif
