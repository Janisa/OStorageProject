/***********************************************************************************
 * 文 件 名   : os_initmodel.h
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年6月23日
 * 文件描述   : 构建初始化函数表模块头文件
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   : 
 * 修改日志   : 
***********************************************************************************/
#include "os_log.h"
#if 0
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#endif

typedef void (*init_call)(void);

#define _init __attribute__((unused, section(".FSymTab")))
#define DECLARE_INIT(func) init_call _fn_##func _init = func

void Os_initcalls();

