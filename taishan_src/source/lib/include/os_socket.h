/***********************************************************************************
 * 文 件 名   : os_socket.h
 * 负 责 人   : 卢美宏
 * 创建日期   : 2018年4月1日
 * 文件描述   : 套接字常用操作封装头文件
 * 版权说明   : Copyright (c) 2008-2018   xx xx xx xx 技术有限公司
 * 其    他   :
 * 修改日志   :
***********************************************************************************/

#ifndef __SOECKET_H__
#define __SOECKET_H__

/*
s_add.sin_family=AF_INET;
s_add.sin_addr.s_addr=htonl(INADDR_ANY) or inet_addr("192.168.1.2");;
s_add.sin_port=htons(portnum);
SOCK_STREAM
*/
int socket_init(int domain, int type, int protocol, struct sockaddr_in *pAddress , char *pstrIp);
int socket_bind(int socket_fd, struct sockaddr_in pAddress);
int socket_listen(int socket_fd, ssize_t maxcount);
int socket_connect(int socket_fd, struct sockaddr_in pAddress);
int socket_accept(int socket_fd, struct sockaddr_in *cliaddr);
ssize_t socket_recv(int fd, void *buf, size_t count);
ssize_t socket_send(int fd, const void *buf, size_t count);
ssize_t socket_recv_exact_nbytes(int fd, void *buf, size_t count);
ssize_t socket_send_exact_nbytes(int fd, const void *buf, size_t count);
int socket_exit(int fd);

#endif