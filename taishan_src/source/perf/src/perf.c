#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#include<event.h>
#include<event2/util.h>
#include "os_msghead.h"
#include "os_socket.h"
#include "msg_handle.h"
#include "msg_defins.h"
#include "os_log.h"

void cmd_msg_cb(int fd, short events, void*arg);
void socket_read_cb(int fd, short events, void*arg);
int main(int argc, char**argv)
{
    if(argc < 3)
    {
        OS_LOG(LOG_ERROR,"please input 2 parameter\n");
        return -1;
    }

    //2 params, IP, port
    int sockfd = tcp_connect_server(argv[1], atoi(argv[2]));
    if(sockfd == -1)
    {
        perror("tcp_connect error\n");
        return -1;
    }
    OS_LOG(LOG_ERROR,"connect to server successful!\n");
    //
    struct event_base* base = event_base_new();
    struct event *ev_sockfd = event_new(base, sockfd, EV_READ | EV_PERSIST, socket_read_cb, NULL);
    event_add(ev_sockfd, NULL);

    //监听终端输入事件
    struct event* ev_cmd = event_new(base, STDIN_FILENO, EV_READ | EV_PERSIST, cmd_msg_cb, (void*)&sockfd);
    event_add(ev_cmd, NULL);

    //
    event_base_dispatch(base);

    OS_LOG(LOG_ERROR,"finished!\n");
    return 0;
}

/*
 * callback,
 */
void socket_read_cb(int fd, short events, void*arg)
{
    char pmsg[1024];

    //
    int len = read(fd, pmsg, sizeof(pmsg) - 1);
    if(len <= 0)
    {
        perror("read fail\n");
        exit(1);
    }
    pmsg[len] = '\0';
    OS_LOG(LOG_ERROR,"recv %s from server\n", pmsg);
}

/*
 * callback,send msg to server
 */
void cmd_msg_cb(int fd, short events, void*arg)
{
    char msg1[1024] = {0};
    char buf[MSG_DATA_LEN];
    s32 iRet = 0;
    int iLen = read(fd, msg1, sizeof(msg1));
    if(iLen <= 0)
    {
        perror("read fail\n");
        exit(1);
    }

    int sockfd = *((int*)arg);
    MSGHEAD msg2;
    OS_MsgInit(msg2);
    msg2.uiSendPid  = 1;
    msg2.uiRecvPid  = 2;
    msg2.uiSendNode = 3;
    msg2.uiRecvNode = 4;
    msg2.uiOperCode = 5;
    msg2.uiRquesFun = 6;
    msg2.RequesData = NULL;
    msg2.uiMsgBuffLen = iLen;
    memset(buf, 0, MSG_DATA_LEN);
    iRet = Os_EncryptMsg(buf, &msg2, msg1, iLen);

//    //把终端的消息发送给服务器
//    //为了简单起见，不考虑写一半数据的情况

    iLen = socket_send_exact_nbytes(sockfd, buf, iRet);
    OS_LOG(LOG_INFO,"write:%d,len:", buf,iLen);

}


